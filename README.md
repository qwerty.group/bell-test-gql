# Тестовое задание GraphQL

## Цели

<p>
Демонстрация подхода к построению GraphQL серверов.
В проекте реализованы базовые принципы запросов и мутаций, включена валидация входящих параметров и оптимизация проблемы N+1 запросов.
</p>

### Для удобства проверки

<ul>
<li>Включены автомиграции, требуется наличие пустой базы в пострес</li>
<li>Включено логирование SQL запросов для удобства проверки</li>
</ul>

## Описание требований и настроек

<p>
Для запуска тестового задания необходимо подготовить базу данных,
требования postgresql 11, созданная пустая база данных bell_demo</p>
<p>

### Параметры базы данных по умолчанию 

<ul>
<li>Хост: localhost</li>
<li>Порт: 5432</li>
<li>Пользователь: postgres</li>
<li>Пароль: camunda</li>
<li>База данных: bell_demo</li>
</ul>

### Переопределение переменных доступно через переменные окружения

<ul>
<li>TYPEORM_HOST</li>
<li>TYPEORM_PORT</li>
<li>TYPEORM_USERNAME</li>
<li>TYPEORM_PASSWORD</li>
<li>TYPEORM_DATABASE</li>
</ul>

## Установка

```bash
$ npm install
```

## Запуск приложения

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Запуск e2e теста

```bash
# e2e tests
$ npm run test:e2e
```
