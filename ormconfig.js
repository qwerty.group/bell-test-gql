const isTs = process.env.TS_ENV === "true"
module.exports = {
  type: "postgres",
  host: process.env.TYPEORM_HOST || "localhost",
  port: process.env.TYPEORM_PORT ? Number(process.env.TYPEORM_PORT) : 5432,
  username: process.env.TYPEORM_USERNAME || "postgres",
  password: process.env.TYPEORM_PASSWORD || "camunda",
  database: process.env.TYPEORM_DATABASE || "bell_demo",
  synchronize: true,
  logging: false,
  extra: { max: 50 },
  cli: {
    entitiesDir: "./src",
    migrationsDir: "./src/migration",
    subscribersDir: "./src/subscriber",
  },
  entities: isTs ? ["./src/**/*.entity.ts"] : ["./dist/**/*.entity.js"],
  migrations: isTs ? ["./src/migration/*.ts"] : ["./dist/migration/*.js"],
  subscribers: isTs
    ? ["./src/**/*.subscriber.ts"]
    : ["./dist/**/*.subscriber.js"],
}
