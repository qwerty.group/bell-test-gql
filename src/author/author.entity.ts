import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { BookEntity } from '../book/book.entity';
import { Field, ID, ObjectType } from '@nestjs/graphql';

@ObjectType()
@Entity({ name: 'authors' })
export class AuthorEntity {
  @PrimaryGeneratedColumn('uuid')
  @Field(type => ID, { description: 'Идентикифактор автора' })
  public id: string;

  @Column({ type: 'text', name: 'name' })
  @Field({ description: 'Имя автора (валидатор не менее 6 символов' })
  public name: string;

  @Field(type => [BookEntity], { description: 'Объект книги' })
  @OneToMany(
    type => BookEntity,
    bookEntity => bookEntity.author,
  )
  public books: BookEntity[];
}
