import { Args, Mutation, Parent, Query, ResolveField, Resolver } from '@nestjs/graphql';
import { AuthorService } from './author.service';
import { AuthorEntity } from './author.entity';
import { AuthorCreateInputType } from './dto/author.create.input-type';
import { AuthorUpdateInputType } from './dto/author.update.input-type';
import { AuthorIdInputType } from './dto/author.id.input-type';
import { BookEntity } from '../book/book.entity';

@Resolver(of => AuthorEntity)
export class AuthorResolver {
  constructor(private readonly authorService: AuthorService) {}

  @Query(type => [AuthorEntity])
  public authors(): Promise<AuthorEntity[]> {
    return this.authorService.findAll();
  }

  @Query(type => AuthorEntity)
  public author(
    @Args('author') authorIdInputType: AuthorIdInputType,
  ): Promise<AuthorEntity> {
    return this.authorService.findOneById(authorIdInputType.id);
  }

  @ResolveField()
  public async books(@Parent() parent: AuthorEntity): Promise<BookEntity[]> {
    return parent.books;
  }

  @Mutation(type => AuthorEntity)
  public authorCreate(
    @Args('author') authorCreateInputType: AuthorCreateInputType,
  ) {
    return this.authorService.create(authorCreateInputType.name);
  }

  @Mutation(type => AuthorEntity)
  public authorUpdate(
    @Args('author') authorUpdateInputType: AuthorUpdateInputType,
  ) {
    const { id, name } = authorUpdateInputType;
    return this.authorService.update(id, name);
  }

  @Mutation(type => AuthorEntity)
  public authorRemove(
    @Args('author') authorCreateInputType: AuthorIdInputType,
  ) {
    return this.authorService.remove(authorCreateInputType.id);
  }
}
