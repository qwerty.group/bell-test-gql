import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthorEntity } from './author.entity';
import { In, Repository } from 'typeorm';

@Injectable()
export class AuthorService {
  constructor(
    @InjectRepository(AuthorEntity)
    private readonly authorRepository: Repository<AuthorEntity>,
  ) {}

  public findAll(): Promise<AuthorEntity[]> {
    return this.authorRepository.find();
  }

  public async findOneById(id: string): Promise<AuthorEntity> {
    const author = await this.authorRepository.findOne(id);
    if (!author) {
      throw new NotFoundException(`Author ${id} not found`);
    }
    return author;
  }

  public findByIds(ids: readonly string[]): Promise<AuthorEntity[]> {
    return this.authorRepository.find({
      id: In(ids as string[]),
    });
  }

  public create(name: string): Promise<AuthorEntity> {
    const author = this.authorRepository.create({ name });
    return this.authorRepository.save(author);
  }

  public async update(id: string, name: string) {
    const author = await this.authorRepository.findOne(id);
    if (!author) {
      throw new NotFoundException(`Author ${id} not found`);
    }
    if (name === author.name) {
      return author;
    }
    return this.authorRepository.save({ ...author, name });
  }

  public async remove(id: string) {
    const author = await this.authorRepository.findOne(id);
    if (!author) {
      throw new NotFoundException(`Author ${id} not found`);
    }
    await this.authorRepository.remove(author);
    return { ...author, id };
  }
}
