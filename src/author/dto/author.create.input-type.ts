import { IsString, MinLength } from 'class-validator';
import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class AuthorCreateInputType {
  @IsString()
  @MinLength(6)
  @Field()
  public readonly name: string;
}
