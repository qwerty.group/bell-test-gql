import { Field, InputType } from '@nestjs/graphql';
import { IsUUID } from 'class-validator';

@InputType()
export class AuthorIdInputType {
  @IsUUID('4')
  @Field()
  public readonly id: string;
}
