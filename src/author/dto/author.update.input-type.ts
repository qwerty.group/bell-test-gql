import { Field, InputType } from '@nestjs/graphql';
import { IsOptional, IsString, IsUUID, MinLength } from 'class-validator';

/**
 * Класс AuthorUpdateInputType добавлен как пример для сущностей
 * с вариативным набором изменений
 */
@InputType()
export class AuthorUpdateInputType {
  @IsUUID('4')
  @Field()
  public readonly id: string;

  @IsOptional()
  @IsString()
  @MinLength(5)
  @Field({ nullable: true })
  public readonly name?: string;
}
