import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { AuthorEntity } from '../author/author.entity';
import { Field, ID, Int, ObjectType } from '@nestjs/graphql';

@ObjectType()
@Entity({ name: 'books' })
export class BookEntity {
  @PrimaryGeneratedColumn('uuid')
  @Field(type => ID, { description: 'Идентификатор книги' })
  public id: string;

  @Column({ type: 'text', name: 'name' })
  @Field({ description: 'Название книги, валидатор не менее 1 символа' })
  public name: string;

  @Column({ type: 'int', name: 'page_count' })
  @Field(type => Int, {
    description: 'Число страниц, валидатор не менее 1 страницы',
  })
  public pageCount: number;

  @Column({ name: 'author_id' })
  @Field({ description: 'Идентификатор автора', nullable: true })
  public authorId: string;

  @ManyToOne(
    type => AuthorEntity,
    authorEntity => authorEntity.books,
    { onDelete: 'CASCADE' },
  )
  @JoinColumn({ name: 'author_id' })
  @Field(type => AuthorEntity, { description: 'Объект автора', nullable: true })
  public author: AuthorEntity;
}
