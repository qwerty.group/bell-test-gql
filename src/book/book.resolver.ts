import { Args, Mutation, Parent, Query, ResolveField, Resolver } from '@nestjs/graphql';
import { BookService } from './book.service';
import { BookEntity } from './book.entity';
import { BookIdInputType } from './dto/book.id.input-type';
import { BookCreateInputType } from './dto/book.create.input-type';
import { AuthorEntity } from '../author/author.entity';
import { BookUpdateInputType } from './dto/book.update.input-type';
import DataLoader from 'dataloader';
import { Loader } from 'nestjs-dataloader';
import { AuthorLoader } from '../loader/author.loader';

@Resolver(of => BookEntity)
export class BookResolver {
  constructor(private readonly bookService: BookService) {}

  @Query(type => [BookEntity])
  public books(): Promise<BookEntity[]> {
    return this.bookService.findAll();
  }

  @Query(type => BookEntity)
  public book(
    @Args('book') bookIdInputType: BookIdInputType,
  ): Promise<BookEntity> {
    return this.bookService.findOneById(bookIdInputType.id);
  }

  @ResolveField()
  public author(
    @Parent() parent: BookEntity,
    @Loader(AuthorLoader.name)
    authorLoader: DataLoader<AuthorEntity['id'], AuthorEntity>,
  ): Promise<AuthorEntity> {
    return authorLoader.load(parent.authorId);
  }

  @Mutation(type => BookEntity)
  public bookCreate(
    @Args('book') bookCreateInputType: BookCreateInputType,
  ): Promise<any> {
    const { name, pageCount, authorId } = bookCreateInputType;
    return this.bookService.create(name, pageCount, authorId);
  }

  @Mutation(type => BookEntity)
  public async bookUpdate(
    @Args('book') bookUpdateInputType: BookUpdateInputType,
  ): Promise<BookEntity> {
    return await this.bookService.update(bookUpdateInputType);
  }

  @Mutation(type => BookEntity)
  public async bookRemove(
    @Args('book') bookIdInputType: BookIdInputType,
  ): Promise<BookEntity> {
    return await this.bookService.remove(bookIdInputType.id);
  }
}
