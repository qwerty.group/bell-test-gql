import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BookEntity } from './book.entity';
import { Repository } from 'typeorm';

@Injectable()
export class BookService {
  constructor(
    @InjectRepository(BookEntity)
    private readonly bookRepository: Repository<BookEntity>,
  ) {}

  public findAll(): Promise<BookEntity[]> {
    return this.bookRepository.find();
  }

  public async findOneById(id: string): Promise<BookEntity> {
    const book = await this.bookRepository.findOne(id);
    if (!book) {
      throw new NotFoundException(`Book ${id} not found`);
    }
    return book;
  }

  public findByAuthorId(authorId: string): Promise<BookEntity[]> {
    return this.bookRepository.find({ where: { authorId } });
  }

  public async create(
    name: string,
    pageCount: number,
    authorId: string,
  ): Promise<BookEntity> {
    const book = this.bookRepository.create({ name, authorId, pageCount });
    return this.bookRepository.save(book);
  }

  public async update({
    id,
    ...updatedBook
  }: {
    id: string;
    name?: string;
    pageCount?: number;
    authorId?: string;
  }) {
    const book = await this.bookRepository.findOne(id);
    if (!book) {
      throw new NotFoundException(`Book ${id} not found`);
    }
    return this.bookRepository.save({ ...book, ...updatedBook });
  }

  public async remove(id: string) {
    const book = await this.bookRepository.findOne(id);
    if (!book) {
      throw new NotFoundException(`Book ${id} not found`);
    }
    await this.bookRepository.remove(book);
    return { ...book, id };
  }
}
