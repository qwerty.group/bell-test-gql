import { Field, InputType } from '@nestjs/graphql';
import { IsInt, IsString, MinLength, Min, IsUUID } from 'class-validator';

@InputType()
export class BookCreateInputType {
  @IsString()
  @MinLength(1)
  @Field()
  public readonly name: string;

  @IsInt()
  @Min(1)
  @Field()
  public readonly pageCount: number;

  @IsUUID('4')
  @Field()
  public readonly authorId: string;
}
