import { InputType, Field } from '@nestjs/graphql';
import { IsOptional, IsUUID } from 'class-validator';

@InputType()
export class BookIdInputType {
  @IsOptional()
  @IsUUID('4')
  @Field()
  public readonly id: string;
}
