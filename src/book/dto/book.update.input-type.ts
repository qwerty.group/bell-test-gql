import {
  IsInt,
  IsOptional,
  IsString,
  IsUUID,
  Min,
  MinLength,
} from 'class-validator';
import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class BookUpdateInputType {
  @IsUUID('4')
  @Field()
  public readonly id: string;

  @IsOptional()
  @IsString()
  @MinLength(1)
  @Field({ nullable: true })
  public readonly name: string;

  @IsOptional()
  @IsInt()
  @Min(1)
  @Field({ nullable: true })
  public readonly pageCount: number;

  @IsOptional()
  @IsUUID('4')
  @Field({ nullable: true })
  public readonly authorId: string;
}
