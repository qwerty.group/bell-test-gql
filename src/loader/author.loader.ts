import { Injectable } from '@nestjs/common';
import DataLoader from 'dataloader';
import { AuthorEntity } from '../author/author.entity';
import { AuthorService } from '../author/author.service';

@Injectable()
export class AuthorLoader {
  constructor(private readonly authorService: AuthorService) {}

  public generateDataLoader(): DataLoader<string, AuthorEntity> {
    return new DataLoader<string, AuthorEntity>(keys =>
      this.authorService.findByIds(keys),
    );
  }
}
