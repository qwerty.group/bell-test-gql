import { forwardRef, Module } from '@nestjs/common';
import { AuthorModule } from '../author/author.module';
import { AuthorLoader } from './author.loader';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { DataLoaderInterceptor } from 'nestjs-dataloader';

@Module({
  imports: [forwardRef(() => AuthorModule)],
  providers: [
    AuthorLoader,
    {
      provide: APP_INTERCEPTOR,
      useClass: DataLoaderInterceptor,
    },
  ],
  exports: [AuthorLoader],
})
export class LoaderModule {}
