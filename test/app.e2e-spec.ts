import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import request from 'supertest';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GraphQLModule } from '@nestjs/graphql';
import { LoaderModule } from '../src/loader/loader.module';
import { BookModule } from '../src/book/book.module';
import { AuthorModule } from '../src/author/author.module';
import { BookEntity } from '../src/book/book.entity';
import { AuthorEntity } from '../src/author/author.entity';

const createInputTypeObject = (data: object) =>
  JSON.stringify(data).replace(/\"([^(\")"]+)\":/g, '$1:');

const author = { id: null, name: 'Author Name 1' };
const newAuthorName = 'New ' + author.name;

const book = {
  id: null,
  name: 'Book Name 1',
  pageCount: 100,
};

const bookUpdate = {
  name: 'Book name 2',
  pageCount: 200,
};

let app: INestApplication;
beforeAll(async done => {
  const moduleFixture: TestingModule = await Test.createTestingModule({
    imports: [
      TypeOrmModule.forRoot({
        type: 'postgres',
        host: process.env.TYPEORM_HOST || 'localhost',
        port: process.env.TYPEORM_PORT
          ? Number(process.env.TYPEORM_PORT)
          : 5432,
        username: process.env.TYPEORM_USERNAME || 'postgres',
        password: process.env.TYPEORM_PASSWORD || 'camunda',
        database: process.env.TYPEORM_DATABASE || 'bell_demo',
        synchronize: true,
        logging: false,
        entities: [BookEntity, AuthorEntity],
      }),
      GraphQLModule.forRoot({
        autoSchemaFile: 'schema.gql',
        playground: true,
      }),
      LoaderModule,
      BookModule,
      AuthorModule,
    ],
  }).compile();
  app = moduleFixture.createNestApplication();
  await app.init();
  done();
});

afterAll(async done => {
  await app.close();
  done();
});

test('Mutation authorCreate', () => {
  const { id, ...authorData } = author;
  const createAuthorMutation = `
      mutation {
        authorCreate(author: ${createInputTypeObject(authorData)}) { id, name }
      }
    `;
  return request(app.getHttpServer())
    .post('/graphql')
    .send({
      operationName: null,
      query: createAuthorMutation,
    })
    .expect(({ body }) => {
      const data = body.data.authorCreate;
      expect(data.name).toBe(author.name);
      expect(typeof data.id).toBe('string');
      author.id = data.id;
    })
    .expect(200);
});

test('Query Get Author by id after create', () => {
  const { id } = author;
  const removeAuthorMutation = `
      query {
        author(author:${createInputTypeObject({ id })}) { id, name }
      }
    `;
  return request(app.getHttpServer())
    .post('/graphql')
    .send({
      operationName: null,
      query: removeAuthorMutation,
    })
    .expect(({ body }) => {
      const data = body.data.author;
      expect(data.id).toBe(author.id);
      expect(data.name).toBe(author.name);
    })
    .expect(200);
});

test('Mutation Author Update', () => {
  const updateAuthorMutation = `
      mutation {
        authorUpdate(author: ${createInputTypeObject({
          ...author,
          name: newAuthorName,
        })}) { id, name }
      }
    `;
  return request(app.getHttpServer())
    .post('/graphql')
    .send({
      operationName: null,
      query: updateAuthorMutation,
    })
    .expect(({ body }) => {
      const data = body.data.authorUpdate;
      expect(data.name).toBe(newAuthorName);
    })
    .expect(200);
});

test('Book create', () => {
  const { id, ...bookData } = book;
  const createBookMutation = `
      mutation {
        bookCreate(book: ${createInputTypeObject({
          ...bookData,
          authorId: author.id,
        })}) { id, name, pageCount, author {id, name} }
      }
    `;
  return request(app.getHttpServer())
    .post('/graphql')
    .send({
      operationName: null,
      query: createBookMutation,
    })
    .expect(({ body }) => {
      const data = body.data.bookCreate;
      expect(data.name).toBe(book.name);
      expect(data.pageCount).toBe(book.pageCount);
      expect(typeof data.id).toBe('string');
      expect(typeof data.author.id).toBe('string');
      expect(data.author.id).toBe(author.id);
      book.id = data.id;
    })
    .expect(200);
});

test('Book update', () => {
  const updateBookMutation = `
      mutation {
        bookUpdate(book: ${createInputTypeObject({
          ...book,
          ...bookUpdate,
        })}) { id, name, pageCount }
      }
    `;
  return request(app.getHttpServer())
    .post('/graphql')
    .send({
      operationName: null,
      query: updateBookMutation,
    })
    .expect(({ body }) => {
      const data = body.data.bookUpdate;
      expect(data.name).toBe(bookUpdate.name);
      expect(data.pageCount).toBe(bookUpdate.pageCount);
    })
    .expect(200);
});

test('Book with author', () => {
  const { id, ...authorData } = author;
  const createAuthorMutation = `
      mutation {
        authorCreate(author: ${createInputTypeObject(authorData)}) { id, name }
      }
    `;
  return request(app.getHttpServer())
    .post('/graphql')
    .send({
      operationName: null,
      query: createAuthorMutation,
    })
    .expect(({ body }) => {
      const data = body.data.authorCreate;
      expect(data.name).toBe(author.name);
      expect(typeof data.id).toBe('string');
    })
    .expect(200);
});

test('Mutation Author cascade delete', () => {
  const { id } = author;
  const removeAuthorMutation = `
      mutation {
        authorRemove(author: ${createInputTypeObject({ id })}) { id, name }
      }
    `;
  return request(app.getHttpServer())
    .post('/graphql')
    .send({
      operationName: null,
      query: removeAuthorMutation,
    })
    .expect(({ body }) => {
      const data = body.data.authorRemove;
      expect(data.name).toBe(newAuthorName);
      expect(typeof data.id).toBe('string');
      author.id = data.id;
    })
    .expect(200);
});

test('Query Get Author by id after cascade delete', () => {
  const { id } = author;
  const removeAuthorMutation = `
      query {
        author(author: ${createInputTypeObject({ id })}) { id, name }
      }
    `;
  return request(app.getHttpServer())
    .post('/graphql')
    .send({
      operationName: null,
      query: removeAuthorMutation,
    })
    .expect(({ body }) => {
      const { data, errors } = body;
      expect(data).toBe(null);
      expect(typeof errors).toBe('object');
      author.id = null;
    })
    .expect(200);
});

test('Query Get Book by id after cascade delete', () => {
  const { id } = book;
  const removeAuthorMutation = `
      query {
        book(book: ${createInputTypeObject({
          id,
        })}) { id, name, pageCount, authorId }
      }
    `;
  return request(app.getHttpServer())
    .post('/graphql')
    .send({
      operationName: null,
      query: removeAuthorMutation,
    })
    .expect(({ body }) => {
      const { data, errors } = body;
      expect(data).toBe(null);
      expect(typeof errors).toBe('object');
      author.id = null;
    })
    .expect(200);
});
